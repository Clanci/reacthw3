import PropTypes from 'prop-types';
import { useState, useEffect, useContext } from 'react';
import Card from '../Cards/Card';
import { CountContext } from '../../context/countContext';

export default function Fav({ itemData }) {
    const [favoriteItems, setFavoriteItems] = useState([]);
    const {favoriteCount} = useContext(CountContext);

    useEffect(() => {
        const favoriteItemsFromStorage = JSON.parse(localStorage.getItem('favorites')) || [];
        setFavoriteItems(favoriteItemsFromStorage);
    }, [favoriteCount]);

    return (
        <section className="favoritePages">
        <h2>Your Favorites</h2>
        <div className="favoriteItems item-container">
            {favoriteItems.map((sku) => {
            const favoriteItemData = itemData.find((item) => item.sku === sku);
            if (favoriteItemData) {
                return <Card key={sku} {...favoriteItemData} inFavorite />;
            }
            return null; 
            })}
        </div>
        </section>
    );
}

Fav.propTypes = {
    itemData: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string.isRequired,
            price: PropTypes.number.isRequired,
            image: PropTypes.string.isRequired,
            article: PropTypes.string.isRequired,
            color: PropTypes.string.isRequired,
        })
    ).isRequired,
};